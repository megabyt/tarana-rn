import React from 'react';
import * as Sentry from '@sentry/react-native';

import AppNavigator from '~components/Navigator/MainNavigator';

const routingInstrumentation = new Sentry.ReactNavigationInstrumentation();
Sentry.init({
  dsn: 'https://0ae997adba53473c8a4ac5d32def9ab2@o1134608.ingest.sentry.io/6182238',
  tracesSampleRate: 1.0,
  integrations: [
    new Sentry.ReactNativeTracing({
      routingInstrumentation
    })
  ]
});

const App = () => <AppNavigator routingInstrumentation={routingInstrumentation} />;
export default Sentry.wrap(App);
