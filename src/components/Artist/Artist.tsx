import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import MbText from '~components/shared/MbText/MbText';
import MbView from '~components/shared/MbView/MbView';
import { DataEntity } from '~screens/Home/cleanedHomeData.types';
import playerService from '~services/player/player';
import { globalStyles } from '~styles/global';
import { styles } from './artist.styles';

const Artist = ({ item }: { item: DataEntity }) => (
  <TouchableOpacity
    activeOpacity={0.8}
    onPress={() => {
      playerService.playItem(item);
    }}
  >
    <View style={styles.mainContainer}>
      <MbView style={styles.posterContainer}>
        <FastImage
          style={styles.imageStyles}
          source={{
            uri: item.icon_image,
            priority: FastImage.priority.normal
          }}
          resizeMode={FastImage.resizeMode.cover}
        />
      </MbView>
      <MbText numberOfLines={1} style={[globalStyles.textStyle, styles.titleStyle]}>
        {item.name}
      </MbText>
    </View>
  </TouchableOpacity>
);

export default Artist;
