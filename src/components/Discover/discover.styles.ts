import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    width: 240,
    borderRadius: 6,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 4
  },
  posterContainer: {
    width: 240,
    height: 120,
    overflow: 'hidden',
    borderRadius: 6
  },
  borderColor: {
    borderColor: '#e7e7e7',
    borderWidth: 0.4
  },
  imageStyles: {
    width: 240,
    height: 120
  },
  titleStyle: {
    display: 'flex',
    // alignSelf: 'center',
    paddingVertical: 4
  },
  titleContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
