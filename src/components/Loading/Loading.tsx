import AnimatedLottieView from 'lottie-react-native';
import React from 'react';
import { View } from 'react-native';
import { styles } from './loading.styles';

const Loading = () => {
  return (
    <View style={styles.mainLoadingContainer}>
      <AnimatedLottieView
        style={styles.animationStyle}
        source={require('../../../assets/loading4.json')}
        autoPlay
        loop
      />
    </View>
  );
};

export default Loading;
