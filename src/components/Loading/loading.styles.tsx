import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  mainLoadingContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  animationStyle: {
    height: '100%',
    width: '100%'
  }
});
