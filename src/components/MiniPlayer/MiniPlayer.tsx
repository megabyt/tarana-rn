import { LinearGradient } from 'expo-linear-gradient';
import { Heart, Pause, Play } from 'iconsax-react-native';
import React, { SetStateAction, useEffect, useState } from 'react';
import { ActivityIndicator, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import { State, usePlaybackState } from 'react-native-track-player';
import Player from '~components/Player/Player';
import MbText from '~components/shared/MbText/MbText';
import { COLORS } from '~config/config';
import playerService from '~services/player/player';
import useStore from '~states/useStore';
import { globalStyles } from '~styles/global';
import { getColors } from '~utils/helpers';
import { styles } from './miniplayer.styles';

const MiniPlayer = () => {
  const { theme: currentTheme, currentTrack } = useStore((state) => state);
  const [colors, setColors] = useState<SetStateAction<any>>({});
  const state = usePlaybackState();

  const miniPlayerStyle = currentTheme === 'dark' ? styles.miniPlayerDark : styles.miniPlayerLight;
  const { isPlayerModalVisible, setIsPlayerModalVisible } = useStore((state) => state);

  const isPlaying = state === State.Playing;
  const isLoading = state === State.Connecting || state === State.Buffering;

  useEffect(() => {
    if (currentTrack.posterUrl) {
      getColors(currentTrack.posterUrl).then((data) => setColors(data));
    }
  }, [currentTrack.posterUrl]);

  return (
    <LinearGradient
      colors={[colors.dominant ?? '#20005e', colors.darkMuted ?? '#873535']}
      start={{ x: 0, y: 0 }}
      end={{ x: 1.5, y: 0 }}
    >
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => setIsPlayerModalVisible(!isPlayerModalVisible)}
        style={[styles.miniPlayerContainer, miniPlayerStyle]}
      >
        <View style={styles.miniPlayerImageContainer}>
          <FastImage
            style={styles.miniPlayerImage}
            source={{
              uri: currentTrack.icon_image as string,
              priority: FastImage.priority.normal
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </View>

        <View style={styles.playerMetaContainer}>
          <MbText style={[globalStyles.cricketHeadingStyle, styles.miniPlayerText]}>
            {currentTrack.name}
          </MbText>
          <MbText style={[globalStyles.textStyle, styles.miniPlayerText]}>Sajan Bin</MbText>
        </View>
        <View style={styles.iconContainer}>
          <TouchableOpacity activeOpacity={0.8}>
            <Heart
              size={36}
              color={COLORS.primary[900]}
              variant="Bold"
              style={styles.favoriteIcon}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              playerService.togglePlay();
            }}
            activeOpacity={0.8}
          >
            {isLoading ? (
              <ActivityIndicator size="large" color={COLORS.primary[900]} />
            ) : isPlaying ? (
              <Pause size={36} color={COLORS.dark[50]} variant="Bold" />
            ) : (
              <Play size={36} color={COLORS.dark[50]} variant="Bold" />
            )}
          </TouchableOpacity>
        </View>
        <Player gradientColors={colors} currentTrack={currentTrack} />
      </TouchableOpacity>
    </LinearGradient>
  );
};
export default React.memo(MiniPlayer);
