import { StyleSheet } from 'react-native';
import { COLORS } from '~config/config';

export const styles = StyleSheet.create({
  miniPlayerContainer: {
    height: 55,
    width: '100%',
    borderTopWidth: 0.2,
    borderBottomWidth: 0.2,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    overflow: 'hidden'
  },
  miniPlayerDark: {
    // backgroundColor: '',
    borderTopColor: COLORS.dark[700],
    borderBottomColor: COLORS.dark[700]
  },
  miniPlayerLight: {
    // backgroundColor: '#ffffff',
    borderTopColor: COLORS.dark[50],
    borderBottomColor: COLORS.dark[50]
  },
  miniPlayerImageContainer: {
    height: 50,
    width: 50,
    borderRadius: 4,
    overflow: 'hidden'
  },
  miniPlayerImage: {
    height: 50,
    width: 50
  },
  playerMetaContainer: {
    width: '60%',
    height: '100%',
    marginHorizontal: 4,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  iconContainer: {
    width: '20%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  favoriteIcon: {
    marginRight: 6
  },
  miniPlayerText: {
    color: 'white'
  }
});
