import { BottomTabBar, createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import * as React from 'react';
import { StatusBar } from 'react-native';
import { QueryClientProvider, QueryClient } from 'react-query';
import MiniPlayer from '~components/MiniPlayer/MiniPlayer';
import { COLORS } from '~config/config';
import News from '~screens/News/News';
import Search from '~screens/Search/Search';
import Settings from '~screens/Settings/Settings';
import useStore from '~states/useStore';
import { globalStyles } from '~styles/global';
import { MbDark, MbLight, tabBarIconSelector } from '~utils/themeUtils';
import HomeStackNavigator from './StackNavigators/HomeStackNavigator';

const Tab = createBottomTabNavigator();
const queryClient = new QueryClient({
  defaultOptions: { queries: { retry: 0 } }
});

export default function AppNavigator({ routingInstrumentation }: { routingInstrumentation: any }) {
  const theme = useStore((state) => state.theme);
  const needMiniPlayer = useStore((state) => state.needMiniPlayer);

  const isDarkTheme = theme === 'dark';

  StatusBar.setBackgroundColor(isDarkTheme ? 'rgba(0, 0, 0, 0.3)' : 'rgba(231,231,231,0.3)');
  StatusBar.setBarStyle(isDarkTheme ? 'light-content' : 'dark-content');
  StatusBar.setTranslucent(true);
  const navigation = React.useRef();

  return (
    <QueryClientProvider client={queryClient}>
      <NavigationContainer
        theme={isDarkTheme ? MbDark : MbLight}
        onReady={() => {
          routingInstrumentation.registerNavigationContainer(navigation);
        }}
      >
        <Tab.Navigator
          tabBar={(tabsProps) => (
            <>
              {needMiniPlayer && <MiniPlayer />}
              <BottomTabBar {...tabsProps} />
            </>
          )}
          screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) =>
              tabBarIconSelector(route, {
                focused,
                color,
                size
              }),
            tabBarActiveTintColor: COLORS.primary[900],
            tabBarInactiveTintColor: isDarkTheme ? 'rgb(159,159,150)' : COLORS.dark[600],
            tabBarStyle: {
              borderTopColor: '#e7e7e718',
              backgroundColor: isDarkTheme ? '#000' : '#fff',
              elevation: 3
            },
            tabBarLabelStyle: globalStyles.tabBarLabelStyles,
            headerShown: false,
            lazy: true
          })}
        >
          <Tab.Screen name="Home" component={HomeStackNavigator} />
          <Tab.Screen name="Search" component={Search} />
          <Tab.Screen name="News" component={News} />
          <Tab.Screen name="Settings" component={Settings} />
        </Tab.Navigator>
      </NavigationContainer>
    </QueryClientProvider>
  );
}
