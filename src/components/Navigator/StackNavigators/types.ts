export type RootStackParamList = {
  HomeScreen: undefined;
  Favorites: undefined;
  Player: { route: any } | undefined;
  Feed: { sort: 'latest' | 'top' } | undefined;
};
