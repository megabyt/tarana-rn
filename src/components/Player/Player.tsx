import { Slider } from '@miblanchard/react-native-slider';
import { LinearGradient } from 'expo-linear-gradient';
import {
  Next,
  PauseCircle,
  PlayCircle,
  Previous,
  VolumeHigh,
  VolumeLow
} from 'iconsax-react-native';
import React from 'react';
import { ActivityIndicator, Dimensions, TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import Modal from 'react-native-modal';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { State, usePlaybackState, useProgress } from 'react-native-track-player';
import MbText from '~components/shared/MbText/MbText';
import MbView from '~components/shared/MbView/MbView';
import { COLORS } from '~config/config';
import Main from '~screens/Main';
import playerService from '~services/player/player';
import useStore from '~states/useStore';
import { globalStyles } from '~styles/global';
import { getTimeDuration } from '~utils/helpers';
import ChevronLeft from '../../../assets/icons/chevron-left.svg';
import MoreVertical from '../../../assets/icons/more-vertical.svg';
import { styles } from './player.styles';
import { CurrentTrackType, GradientColorTypes } from './types';

function Player({
  gradientColors,
  currentTrack
}: {
  gradientColors: GradientColorTypes;
  currentTrack: CurrentTrackType;
}) {
  const {
    isPlayerModalVisible,
    setIsPlayerModalVisible,
    setPlayerVolume,
    playerVolume,
    setPlayerProgress
  } = useStore((state) => state);

  const playerState = usePlaybackState();
  const isPlaying = playerState === State.Playing;
  const isLoading = playerState === State.Connecting || playerState === State.Buffering;
  const progress = useProgress();

  return (
    <Modal
      isVisible={isPlayerModalVisible}
      statusBarTranslucent
      backdropOpacity={0.5}
      backdropColor="#000"
      coverScreen={true}
      deviceHeight={Dimensions.get('screen').height}
      animationIn="zoomIn"
      animationOut="zoomOut"
      style={styles.modalStyles}
      animationInTiming={150}
      animationOutTiming={100}
      onBackButtonPress={() => setIsPlayerModalVisible(false)}
      useNativeDriver
      useNativeDriverForBackdrop
    >
      <MbView style={styles.mainContainerAndGradient}>
        <LinearGradient
          colors={[gradientColors.vibrant ?? '#20005e', gradientColors.darkMuted ?? '#873535']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 0.8 }}
          style={styles.mainContainerAndGradient}
        >
          <Main index={0}>
            <View style={styles.topIcons}>
              <ChevronLeft height={28} width={28} color={COLORS.secondary[50]} />
              <View style={styles.radioTitleContainer}>
                <MbText style={[globalStyles.cricketHeadingStyle, styles.radioTitle]}>
                  {currentTrack.name}
                </MbText>
              </View>
              <MoreVertical height={28} width={28} color={COLORS.secondary[50]} />
            </View>

            <View style={styles.posterBase}>
              <View style={[styles.posterHolder, { backgroundColor: gradientColors.vibrant }]}>
                <FastImage
                  style={styles.poster}
                  source={{
                    uri: currentTrack.posterUrl as string,
                    priority: FastImage.priority.normal
                  }}
                  resizeMode={FastImage.resizeMode.cover}
                />
              </View>
            </View>
            <View style={styles.controlContainer}>
              <View style={styles.seekContainer}>
                <Slider
                  animateTransitions
                  value={progress.position}
                  minimumValue={0}
                  maximumValue={progress.duration}
                  onValueChange={(value) => setPlayerProgress(value)}
                  minimumTrackTintColor={gradientColors.darkMuted && gradientColors.dominant}
                  trackStyle={styles.trackStyle}
                  thumbStyle={styles.thumbStyle}
                />
                <View style={styles.progressContainer}>
                  <MbText style={globalStyles.subTitleStyle}>
                    {getTimeDuration(null, progress.position)}
                  </MbText>
                  <MbText style={globalStyles.subTitleStyle}>
                    {progress.duration === 0
                      ? '∞'
                      : getTimeDuration(progress.duration, progress.position)}
                  </MbText>
                </View>
              </View>
              <View style={styles.playerControlBase}>
                <View style={styles.playerControl}>
                  <TouchableOpacity
                    onPress={() => {
                      playerService.togglePlay();
                    }}
                    activeOpacity={0.8}
                  >
                    <Previous size={hp('3.5%')} color={COLORS.dark[50]} variant="Bold" />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      playerService.togglePlay();
                    }}
                    activeOpacity={0.8}
                  >
                    {isLoading ? (
                      <ActivityIndicator size={hp('12%')} color={COLORS.primary[900]} />
                    ) : isPlaying ? (
                      <PauseCircle size={hp('12%')} color={COLORS.dark[50]} variant="Bold" />
                    ) : (
                      <PlayCircle size={hp('12%')} color={COLORS.dark[50]} variant="Bold" />
                    )}
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      playerService.togglePlay();
                    }}
                    activeOpacity={0.8}
                  >
                    <Next size={hp('3.5%')} color={COLORS.dark[50]} variant="Bold" />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.volumeControlContainer}>
                <VolumeLow size={hp('3%')} color={COLORS.dark[50]} />
                <View style={styles.volumeSliderContainer}>
                  <Slider
                    animateTransitions
                    value={playerVolume}
                    minimumValue={0}
                    maximumValue={1}
                    onValueChange={(value) => {
                      playerService.setVolume(value);
                      setPlayerVolume(value);
                    }}
                    minimumTrackTintColor={gradientColors.darkMuted && gradientColors.dominant}
                    trackStyle={styles.trackStyle}
                    thumbStyle={styles.thumbStyle}
                  />
                </View>
                <VolumeHigh size={hp('3%')} color={COLORS.dark[50]} />
              </View>
            </View>
          </Main>
        </LinearGradient>
      </MbView>
    </Modal>
  );
}

export default Player;
