import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from 'react-native-responsive-screen';
import { COLORS, FONTS } from '~config/config';

export const styles = StyleSheet.create({
  modalStyles: {
    margin: 0,
    padding: 0
  },
  playerContainer: {
    flex: 1,
    margin: 0,
    padding: 0,
    paddingTop: 50
  },
  mainContainerAndGradient: {
    flex: 1
  },
  topIcons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 8,
    marginTop: 12
  },
  posterBase: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 4
  },
  posterHolder: {
    height: hp('35%'),
    width: wp('35%'),
    aspectRatio: 1,
    overflow: 'hidden',
    borderRadius: 8,
    elevation: 8
  },
  poster: {
    height: '100%',
    width: '100%'
  },
  controlContainer: {
    display: 'flex',
    flex: 4,
    justifyContent: 'space-evenly'
    // backgroundColor: 'tomato'
  },
  radioTitleContainer: {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  radioTitle: {
    fontFamily: FONTS.semiB,
    color: COLORS.secondary[50]
  },
  seekContainer: {
    marginHorizontal: 10,
    alignItems: 'stretch',
    justifyContent: 'center'
  },
  trackStyle: {
    backgroundColor: COLORS.secondary[50],
    height: 2
  },
  thumbStyle: {
    backgroundColor: COLORS.secondary[50],
    height: 12,
    width: 12
  },
  progressContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  playerControlBase: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  playerControl: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: '70%'
  },
  volumeControlContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    width: '100%'
  },
  volumeSliderContainer: {
    width: '70%'
  }
});
