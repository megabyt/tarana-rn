export interface GradientColorTypes {
  muted: string;
  lightMuted: string;
  lightVibrant: string;
  dominant: string;
  darkMuted: string;
  darkVibrant: string;
  vibrant: string;
  average: string;
  platform: string;
}

export interface CurrentTrackType {
  url: string;
  name: string;
  posterUrl: string;
  iconUrl: string;
  radio_uid: string;
  icon_image: string;
}
