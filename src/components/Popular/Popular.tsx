import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import MbText from '~components/shared/MbText/MbText';
import MbView from '~components/shared/MbView/MbView';
import { DataEntity } from '~screens/Home/cleanedHomeData.types';
import playerService from '~services/player/player';
import useStore from '~states/useStore';
import { globalStyles } from '~styles/global';
import { styles } from './popular.styles';

const Popular = ({ item }: { item: DataEntity }) => {
  const { name, icon_image } = item;
  const theme = useStore((state) => state.theme);
  const borderColor = theme === 'light' ? styles.borderColor : {};
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => {
        playerService.playItem(item);
        // console.log(playerService.);
      }}
    >
      <View style={styles.mainContainer}>
        <MbView style={[styles.posterContainer, borderColor]}>
          <FastImage
            style={styles.imageStyles}
            source={{
              uri: icon_image as string,
              priority: FastImage.priority.normal
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </MbView>
        <View style={styles.titleContainer}>
          <MbText numberOfLines={1} style={[globalStyles.textStyle, styles.titleStyle]}>
            {name}
          </MbText>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Popular;
