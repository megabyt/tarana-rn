import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    width: 130,
    borderRadius: 6,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    marginHorizontal: 4
  },
  posterContainer: {
    width: 130,
    height: 130,
    overflow: 'hidden',
    borderRadius: 6
  },
  borderColor: {
    borderColor: '#e7e7e7',
    borderWidth: 0.4
  },
  imageStyles: {
    width: 130,
    height: 130
  },
  titleContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleStyle: {
    display: 'flex',
    paddingVertical: 6
  }
});
