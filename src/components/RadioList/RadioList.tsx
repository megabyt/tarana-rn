import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import FastImage from 'react-native-fast-image';
import MbText from '~components/shared/MbText/MbText';
import MbView from '~components/shared/MbView/MbView';
import { DataEntity } from '~screens/Home/cleanedHomeData.types';
import playerService from '~services/player/player';
import { globalStyles } from '~styles/global';
import { styles } from './radiolist.styles';
const RadioList = ({ item }: { item: DataEntity }) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={styles.mainContainer}
      onPress={() => {
        playerService.playItem(item);
      }}
    >
      <View style={styles.mainContainer}>
        <MbView style={[styles.holderContainer]}>
          <View style={styles.listTitleContainer}>
            <MbText numberOfLines={1} style={globalStyles.textStyle}>
              {item.name}
            </MbText>
            <MbText numberOfLines={1} style={globalStyles.subTitleStyle}>
              {item.categories}
            </MbText>
            <MbText numberOfLines={1} style={globalStyles.subTitleStyle}>
              {item.country}
            </MbText>
          </View>
        </MbView>
        <View style={styles.posterContainer}>
          <FastImage
            style={styles.imageStyles}
            source={{
              uri: item.icon_image as string,
              priority: FastImage.priority.normal
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default RadioList;
