import { CloseSquare, SearchNormal1 } from 'iconsax-react-native';
import React, { useEffect } from 'react';
import { Keyboard, TextInput, TouchableOpacity } from 'react-native';
import MbView from '~components/shared/MbView/MbView';
import { COLORS } from '~config/config';
import { DarkOrLight, ThemeSliceType } from '~states/themeSlice/types';
import useStore from '~states/useStore';
import { styles } from './searchbar.styles';

const SearchBar = () => {
  const searchRef = React.createRef<TextInput>();
  const theme = useStore((state) => state.theme) as keyof ThemeSliceType;
  const currentThemeStyles = useStore((state) => state[theme]) as DarkOrLight;
  const color = theme === 'dark' ? 'rgb(177,179,180)' : COLORS.dark[600];
  const { searchTerm, setSearchTerm, setIsKeyboardOpen, setSearchBarFocusStatus } = useStore(
    (state) => state
  );

  useEffect(() => {
    const showSubscription = Keyboard.addListener('keyboardDidShow', () => {
      setIsKeyboardOpen(true);
    });
    const hideSubscription = Keyboard.addListener('keyboardDidHide', () => {
      setIsKeyboardOpen(false);
    });

    return () => {
      showSubscription.remove();
      hideSubscription.remove();
    };
  }, [setIsKeyboardOpen]);

  const onClear = () => {
    searchRef.current?.clear();
    setSearchTerm('');
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const onSubmit = (value: string) => {
    // console.log(value);
  };

  return (
    <MbView style={styles.inputContainerStyle}>
      <TextInput
        ref={searchRef}
        style={[styles.inputStyles, currentThemeStyles.color]}
        placeholder="Search Your Favorite Station"
        placeholderTextColor={color}
        onChangeText={(term) => setSearchTerm(term)}
        value={searchTerm}
        selectionColor={color}
        onSubmitEditing={(event) => onSubmit(event.nativeEvent.text)}
        onFocus={() => setSearchBarFocusStatus(true)}
        onBlur={() => setSearchBarFocusStatus(false)}
      />
      <TouchableOpacity onPress={onClear} activeOpacity={0.8}>
        {searchTerm.length > 0 ? (
          <CloseSquare color={color} variant="Outline" size={28} />
        ) : (
          <SearchNormal1 color={color} variant="Outline" size={28} />
        )}
      </TouchableOpacity>
    </MbView>
  );
};

export default SearchBar;
