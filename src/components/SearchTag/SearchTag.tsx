import { Clock } from 'iconsax-react-native';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import MbText from '~components/shared/MbText/MbText';
import MbView from '~components/shared/MbView/MbView';
import { COLORS } from '~config/config';
import useStore from '~states/useStore';
import { globalStyles } from '~styles/global';
import ArrowLeft from '../../../assets/icons/arrow-up-left.svg';
import { styles } from './search-tag.styles';

const SearchTag = ({ item }: { item: any }) => {
  const currentTheme = useStore((state) => state.theme);
  const searchTagStyle = currentTheme === 'dark' ? styles.searchTagDark : styles.searchTagLight;
  return (
    // eslint-disable-next-line no-console
    <TouchableOpacity activeOpacity={0.8} onPress={() => console.log(item.tag_name)}>
      <MbView style={[styles.searchTagContainer, searchTagStyle]}>
        <View style={styles.tagNameIconContainer}>
          <Clock size={20} color={COLORS.dark[500]} />
          <MbText style={[globalStyles.cricketHeadingStyle, styles.tagStyle]}>
            {item.tag_name}
          </MbText>
        </View>
        <ArrowLeft height={22} width={22} fill={COLORS.dark[500]} />
      </MbView>
    </TouchableOpacity>
  );
};

function arePropsEqual(prevProps: any, nextProps: any) {
  return prevProps.item.tag_name === nextProps.item.tag_name;
}

export default React.memo(SearchTag, arePropsEqual);
