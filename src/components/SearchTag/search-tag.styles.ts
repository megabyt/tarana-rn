import { StyleSheet } from 'react-native';
import { COLORS } from '~config/config';

export const styles = StyleSheet.create({
  searchTagContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 50,
    width: '100%',
    borderBottomWidth: 0.2
  },

  tagNameIconContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },

  searchTagDark: {
    backgroundColor: '#000000',
    borderBottomColor: COLORS.dark[600]
  },

  searchTagLight: {
    backgroundColor: '#ffffff',
    borderBottomColor: COLORS.dark[100]
  },

  tagStyle: {
    paddingLeft: 10
  }
});
