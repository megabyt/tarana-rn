import { ArrowRight2 } from 'iconsax-react-native';
import React, { ReactElement } from 'react';
import { TextProps, View } from 'react-native';
import MbSwitch from '~components/shared/MbSwitch/MbSwitch';
import MbText from '~components/shared/MbText/MbText';
import MbView from '~components/shared/MbView/MbView';
import { COLORS } from '~config/config';
import useStore from '~states/useStore';
import { globalStyles } from '~styles/global';
import { styles } from './settings-option.style';

const SettingsOption = ({
  type,
  icon1,
  icon2,
  content,
  menuTitle
}: {
  type: string;
  content?: TextProps | string;
  menuTitle?: TextProps | string;
  icon1: ReactElement;
  icon2: ReactElement;
}) => {
  const theme = useStore((state) => state.theme);
  const toggleTheme = useStore((state) => state.toggleTheme);
  const currentTheme = theme === 'dark';
  const changeAppTheme = () => {
    const updatedTheme = currentTheme ? 'light' : 'dark';
    toggleTheme(updatedTheme);
  };

  return (
    <View style={styles.optionContainer}>
      <MbView style={[styles.optionStyle]}>
        <View style={styles.iconMainContainer}>
          <MbView style={styles.iconContainerStyle}>
            {type === 'switch' ? (currentTheme ? icon1 : icon2) : icon1}
          </MbView>
        </View>
        <View style={styles.mainTextContainer}>
          <MbText style={styles.settingsTextStyle}>
            {type === 'switch' ? `${currentTheme ? 'Disable' : 'Enable'} Dark Mode` : menuTitle}
          </MbText>
        </View>
        <View style={styles.buttonTextIconContainer}>
          {type === 'switch' ? (
            <MbSwitch callback={changeAppTheme} />
          ) : type === 'text' ? (
            <MbText style={globalStyles.textStyle}>{content}</MbText>
          ) : (
            <ArrowRight2 color={COLORS.dark[500]} size={15} />
          )}
        </View>
      </MbView>
    </View>
  );
};

export default SettingsOption;
