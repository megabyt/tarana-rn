import { StyleSheet } from 'react-native';
import { FONTS } from '~config/config';

export const styles = StyleSheet.create({
  optionContainer: {
    paddingHorizontal: 4,
    marginVertical: 4
  },
  optionStyle: {
    height: 55,
    borderRadius: 8,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },

  iconContainerStyle: {
    borderRadius: 100,
    padding: 10,
    display: 'flex',
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
  },
  settingsTextStyle: {
    fontFamily: FONTS.semiB,
    fontSize: FONTS.sizeR
  },
  iconMainContainer: {
    height: '100%',
    width: '20%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  mainTextContainer: {
    height: '100%',
    width: '60%',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  buttonTextIconContainer: {
    height: '100%',
    width: '20%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
