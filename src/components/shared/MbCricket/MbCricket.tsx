import AnimatedLottieView from 'lottie-react-native';
import React from 'react';
import { View } from 'react-native';
import { useQuery } from 'react-query';
import Loading from '~components/Loading/Loading';
import Error from '~screens/Error/Error';
import { globalStyles } from '~styles/global';
import MbText from '../MbText/MbText';
import MbView from '../MbView/MbView';
import { styles } from './mb-cricket.style';
const getScore = async () => {
  return fetch('https://www.cricbuzz.com/api/cricket-match/commentary/38602').then((res) =>
    res.json()
  );
};
const MbCricket = () => {
  const { isLoading, error, data } = useQuery('scores', getScore, {
    refetchInterval: 2500
  });

  if (isLoading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  const inningsScoreData = data.miniscore.matchScoreDetails.inningsScoreList[0];
  const {
    recentOvsStats,
    currentRunRate,
    batsmanNonStriker,
    batsmanStriker,
    bowlerStriker,
    bowlerNonStriker,
    target,
    status
  } = data.miniscore;

  return (
    <MbView style={styles.mainScoreContainer}>
      <View style={styles.scoreDataContainer}>
        <View>
          <MbText style={globalStyles.cricketHeadingStyle}>
            {inningsScoreData.batTeamName} {}
            {inningsScoreData.score}/{inningsScoreData.wickets} {}- CRR:{' '}
            {parseFloat(currentRunRate)}
          </MbText>
          {target === '' ? null : <MbText style={globalStyles.textStyle}>Target: {target}</MbText>}
        </View>
        <View>
          {data.matchHeader.complete === true ? (
            <MbText style={globalStyles.textSmallBold}>{status}</MbText>
          ) : (
            <AnimatedLottieView
              style={styles.liveAnimationHeight}
              source={require('../../../../assets/live.json')}
              autoPlay
              loop
            />
          )}
        </View>
      </View>
      <View style={styles.batBowlStatsContainer}>
        <View style={styles.batsManDataContainer}>
          <MbText style={[globalStyles.textStyle, styles.textVerticalPadding]}>
            {batsmanStriker.batName}: {batsmanStriker.batRuns} ({batsmanStriker.batBalls})*
          </MbText>
          {batsmanNonStriker.batName !== '' ? (
            <MbText style={globalStyles.textStyle}>
              {batsmanNonStriker.batName}: {batsmanNonStriker.batRuns} ({batsmanNonStriker.batBalls}
              )
            </MbText>
          ) : (
            <></>
          )}
        </View>
        <View style={styles.bowlerDataContainer}>
          <View style={styles.textPaddingLeft}>
            <MbText style={[globalStyles.textStyle, styles.textVerticalPadding]}>
              {bowlerStriker.bowlName}: {bowlerStriker.bowlOvs} (ovr)*
            </MbText>
            {bowlerNonStriker.bowlName !== '' ? (
              <MbText style={globalStyles.textStyle}>
                {bowlerNonStriker.bowlName}: {bowlerNonStriker.bowlOvs} (ovr)
              </MbText>
            ) : (
              <></>
            )}
          </View>
        </View>
      </View>

      <View style={styles.overStatsContainer}>
        <View>
          <MbText style={globalStyles.textSmallBold}>Recent Overs:</MbText>
        </View>
        <View>
          <MbText style={globalStyles.textStyle}>{recentOvsStats}</MbText>
        </View>
      </View>
    </MbView>
  );
};

export default MbCricket;
