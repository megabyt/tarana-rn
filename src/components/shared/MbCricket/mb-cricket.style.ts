import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  mainScoreContainer: {
    height: 140,
    margin: 4,
    borderRadius: 8,
    padding: 10,
    display: 'flex',
    justifyContent: 'space-between'
  },
  scoreDataContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  batBowlStatsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 4
  },
  batsManDataContainer: {
    display: 'flex',
    flex: 1
  },
  bowlerDataContainer: {
    borderColor: 'white',
    borderLeftWidth: 0.2,
    display: 'flex',
    flex: 1,
    alignItems: 'flex-end'
  },
  overStatsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textVerticalPadding: {
    paddingVertical: 4
  },
  textPaddingLeft: {
    paddingLeft: 0
  },
  liveAnimationHeight: {
    height: 16
  }
});
