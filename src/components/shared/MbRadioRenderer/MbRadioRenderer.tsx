import React from 'react';
import { Dimensions, FlatList, TouchableOpacity, View } from 'react-native';
import Artist from '~components/Artist/Artist';
import Discover from '~components/Discover/Discover';
import Popular from '~components/Popular/Popular';
import RadioList from '~components/RadioList/RadioList';
import { CleanedHomeDataEntity0, DataEntity } from '~screens/Home/cleanedHomeData.types';
import { globalStyles } from '~styles/global';
import { getUniqueKey } from '~utils/helpers';
import MbText from '../MbText/MbText';
import { styles } from './mb-radio-renderer.styles';

const renderPopularItem = ({ item }: { item: DataEntity }) => <Popular item={item} />;
const renderDiscover = ({ item }: { item: DataEntity }) => <Discover item={item} />;
const renderArtist = ({ item }: { item: DataEntity }) => <Artist item={item} />;

const MbRadioRenderer = ({ item }: { item: CleanedHomeDataEntity0 }) => {
  const ListTypes = ['gradient', 'circle', 'square'];
  let radioRenderer = renderPopularItem;

  if (item.title === 'Artist') {
    item.type = 'circle';
  }
  if (item.title === 'Regional') {
    item.type = 'gradient';
  }
  switch (item.type) {
    case 'circle':
      radioRenderer = renderArtist;
      break;
    case 'square':
      radioRenderer = renderPopularItem;
      break;
    case 'gradient':
      radioRenderer = renderDiscover;
      break;
    default:
      radioRenderer = renderPopularItem;
  }

  if (item.type === 'ordered_list_block') {
    return (
      <View>
        <View style={styles.mainContainer}>
          <MbText style={globalStyles.titleStyle}>{item.title}</MbText>
          <TouchableOpacity activeOpacity={0.8}>
            <MbText style={globalStyles.subTitleStyle}>See All</MbText>
          </TouchableOpacity>
        </View>
        {item.data.map((item) => (
          <RadioList item={item} key={getUniqueKey()} />
        ))}
      </View>
    );
  }

  if (ListTypes.indexOf(item.type) !== -1) {
    return (
      <React.Fragment
        key={
          new Date().getTime().toString() +
          Math.floor(Math.random() * Math.floor(new Date().getTime())).toString()
        }
      >
        <View style={styles.mainContainer}>
          <MbText style={globalStyles.titleStyle}>{item.title}</MbText>
          <TouchableOpacity activeOpacity={0.8}>
            <MbText style={globalStyles.subTitleStyle}>See All</MbText>
          </TouchableOpacity>
        </View>
        <FlatList
          data={item.data}
          renderItem={radioRenderer}
          horizontal
          showsHorizontalScrollIndicator={false}
          initialNumToRender={4}
          keyExtractor={getUniqueKey}
          maxToRenderPerBatch={Math.ceil(Dimensions.get('window').width / 140)}
          contentContainerStyle={styles.listContainerStyle}
        />
      </React.Fragment>
    );
  }
  return <></>;
};

export default MbRadioRenderer;
