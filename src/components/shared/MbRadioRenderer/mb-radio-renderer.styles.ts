import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 4
  },
  listContainerStyle: {
    height: 160,
    display: 'flex',
    alignItems: 'center'
  }
});
