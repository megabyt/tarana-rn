import React from 'react';
import ToggleSwitch from 'toggle-switch-react-native';
import useStore from '~states/useStore';
import { styles } from './mb-switch.style';

const MbSwitch = ({ callback }: { callback: () => void }) => {
  const theme = useStore((state) => state.theme);
  const currentTheme = theme === 'dark';

  return (
    <ToggleSwitch
      isOn={currentTheme}
      onColor="rgb(26, 26, 26)"
      offColor="rgb(231,234,239)"
      onToggle={callback}
      thumbOffStyle={styles.thumbOffStyle}
      thumbOnStyle={styles.thumbOffStyle}
      trackOffStyle={styles.trackOffStyle}
      trackOnStyle={styles.trackOnStyle}
    />
  );
};

export default MbSwitch;
