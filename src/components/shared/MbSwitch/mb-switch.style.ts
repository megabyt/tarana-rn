import { StyleSheet } from 'react-native';
import { COLORS } from '~config/config';

export const styles = StyleSheet.create({
  thumbOffStyle: {
    height: 25,
    width: 25,
    borderRadius: 100
  },
  thumbOnStyle: {
    height: 25,
    width: 25,
    borderRadius: 100,
    backgroundColor: COLORS.dark[900]
  },
  trackOffStyle: {
    height: 30,
    width: 50,
    borderColor: 'rgb(159,159,159)',
    borderWidth: 1
  },
  trackOnStyle: {
    height: 30,
    width: 50,
    backgroundColor: COLORS.dark[700]
  }
});
