import AnimatedLottieView from 'lottie-react-native';
import React from 'react';
import { View } from 'react-native';

const Favorites = () => {
  return (
    <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
      {/* <Text style={{ color: colors.text }}>Favorite Screen</Text> */}
      <AnimatedLottieView
        style={{ height: '100%', width: '100%' }}
        source={require('../../../assets/loading.json')}
        autoPlay
        loop
      />
    </View>
  );
};

export default Favorites;
