import React from 'react';
import { Dimensions, FlatList } from 'react-native';
import { useQuery } from 'react-query';
import Loading from '~components/Loading/Loading';
import SearchBar from '~components/SearchBar/SearchBar';
import MbRadioRenderer from '~components/shared/MbRadioRenderer/MbRadioRenderer';
import Error from '~screens/Error/Error';
import Main from '~screens/Main';
import { api } from '~services/network/api';
import { getUniqueKey, objToSortedArr } from '~utils/helpers';
import { CleanedHomeData, CleanedHomeDataEntity0 } from './cleanedHomeData.types';
import { HomeData } from './types';

const getData = () => {
  return api.get('station/home/', {
    limit: 15,
    latitude: 12.972442,
    longitude: 77.580643,
    user_id: 'mb-1582446105-hf3453s6sdf'
  });
};

const Home = () => {
  const { isLoading, data } = useQuery('home', getData);
  let cleanedHomeData: CleanedHomeData[] = [];
  const homeData = data?.data as HomeData;

  if (!isLoading && data) {
    cleanedHomeData = objToSortedArr(homeData);
  }

  if (isLoading) {
    return <Loading />;
  }

  if (
    (homeData && homeData.status === 'error') ||
    data?.status === null ||
    data?.problem !== null
  ) {
    return <Error />;
  }

  return (
    <React.Fragment>
      <FlatList
        data={Object.keys(cleanedHomeData)}
        renderItem={({ index }) => {
          return (
            <Main index={index}>
              {index === 0 ? (
                <>
                  <SearchBar />
                  {/* <MbCricket /> */}
                </>
              ) : (
                <></>
              )}
              <MbRadioRenderer item={cleanedHomeData[index] as CleanedHomeDataEntity0} />
            </Main>
          );
        }}
        keyExtractor={getUniqueKey}
        maxToRenderPerBatch={Math.ceil(Dimensions.get('screen').height / 140)}
        initialNumToRender={Math.ceil(Dimensions.get('screen').height / 140)}
        showsVerticalScrollIndicator={false}
        windowSize={15}
      />
    </React.Fragment>
  );
};

export default React.memo(Home);
