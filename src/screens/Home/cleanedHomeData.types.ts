/* eslint-disable no-use-before-define */
export interface CleanedHomeDataEntity0 {
  status: boolean;
  title: string;
  length: number;
  data: DataEntity[];
  order: number;
  type: string;
}
export interface DataEntity {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: number;
  frequency: string;
  categories: string;
  website: string;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
  uid: string;
  tag_name: string;
}
export interface CleanedHomeDataEntity1 {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: number;
  frequency: string;
  categories: string;
  website: string;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
  uid: string;
  tag_name: string;
}
export type CleanedHomeData = CleanedHomeDataEntity0 | boolean;
