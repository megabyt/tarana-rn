/* eslint-disable no-use-before-define */
export interface HomeData {
  status: boolean | string;
  random_stations: RandomStations;
  top_stations: TopStations;
  regional_stations: RegionalStations;
  nearby_stations: NearbyStations;
  user_favorites: UserFavoritesOrUserRecentlyPlayedOrIplStations;
  user_recently_played: UserFavoritesOrUserRecentlyPlayedOrIplStations;
  artist_stations: ArtistStations;
  ipl_stations: UserFavoritesOrUserRecentlyPlayedOrIplStations;
}
export interface RandomStations {
  status: boolean;
  title: string;
  length: number;
  data: DataEntity[] | null;
  order: number;
  type: string;
}
export interface DataEntity {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: null;
  frequency: null;
  categories: string;
  website: string | null;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
}
export interface TopStations {
  status: boolean;
  title: string;
  length: number;
  data: DataEntity1[] | null;
  order: number;
  type: string;
}
export interface DataEntity1 {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: number;
  frequency: string | null;
  categories: string;
  website: string | null;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
}
export interface RegionalStations {
  status: boolean;
  title: string;
  length: number;
  data: DataEntity2[] | null;
  order: number;
  type: string;
}
export interface DataEntity2 {
  uid: string;
  name: string;
  tag_name: string;
  popularity: number;
}
export interface NearbyStations {
  status: boolean;
  title: string;
  length: number;
  data: DataEntity3[] | null;
  order: number;
  type: string;
}
export interface DataEntity3 {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: number;
  frequency: string | null;
  categories: string;
  website: string;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
}
export interface UserFavoritesOrUserRecentlyPlayedOrIplStations {
  status: boolean;
  title: string;
  length: number;
  data: DataEntity4[] | null;
  order: number;
  type: string;
}
export interface DataEntity4 {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: number;
  frequency: string;
  categories: string;
  website: string;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
}
export interface ArtistStations {
  status: boolean;
  title: string;
  length: number;
  data: DataEntity5[] | null;
  order: number;
  type: string;
}
export interface DataEntity5 {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: number | null;
  frequency: string | null;
  categories: string;
  website: string | null;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
}
