import React, { ReactElement } from 'react';
import { StyleSheet, View } from 'react-native';

const styles = StyleSheet.create({
  layoutStyles: {
    display: 'flex',
    flex: 1
  }
});
const Main = ({ children, index }: { children: ReactElement | ReactElement[]; index: number }) => {
  const paddingTop = index === 0 ? 45 : 8;
  return (
    <>
      <View style={[styles.layoutStyles, { paddingTop }]}>{children}</View>
    </>
  );
};

export default Main;
