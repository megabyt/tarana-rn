import React, { useMemo } from 'react';
import { View } from 'react-native';
import { useQuery } from 'react-query';
import Loading from '~components/Loading/Loading';
import SearchBar from '~components/SearchBar/SearchBar';
import Main from '~screens/Main';
import { api } from '~services/network/api';
import useStore from '~states/useStore';
import { arrFilter, orderArrBy } from '~utils/helpers';
import { styles } from './search-screen.styles';
import SearchListRenderer from './SearchListRenderer';
import { TagDataEntity, TagTypes } from './types';

const getData = () => {
  return api.get('station/get-tags');
};

const Search = () => {
  const { isLoading, data } = useQuery('search', getData);
  const rawSearchTags = data?.data as TagTypes;

  let orderedTags: TagDataEntity[] = [];
  if (!isLoading && rawSearchTags) {
    orderedTags = orderArrBy(rawSearchTags.data, 'popularity', 'desc');
  }
  const searchTerm = useStore((state) => state.searchTerm);
  const searchedTag = useMemo(() => arrFilter(orderedTags, searchTerm), [searchTerm]);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <Main index={0}>
      <View>
        <SearchBar />
      </View>
      <View style={styles.listRendererContainer}>
        <SearchListRenderer data={searchedTag} height={50} />
      </View>
    </Main>
  );
};

export default Search;
