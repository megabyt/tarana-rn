import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { DataProvider, LayoutProvider, RecyclerListView } from 'recyclerlistview';
import SearchTag from '~components/SearchTag/SearchTag';

interface SearchListProps {
  data: any;
  height: number;
}

class SearchListRenderer extends Component<SearchListProps> {
  constructor(props: any) {
    super(props);
  }
  height = this.props.height || 50;

  dataProvider = new DataProvider((r1, r2) => r1 !== r2);

  layoutProvider = new LayoutProvider(
    (index) => {
      return index;
    },
    (type, dim) => {
      dim.width = Dimensions.get('screen').width;
      dim.height = this.height;
    }
  );

  rowRenderer = (type: any, data: any) => {
    return <SearchTag item={data} />;
  };

  render() {
    const { data }: { data: any } = this.props;

    if (!data) {
      return <></>;
    }

    if (data.length > 0) {
      this.dataProvider = this.dataProvider.cloneWithRows(data);
    }

    return (
      <RecyclerListView
        dataProvider={this.dataProvider}
        layoutProvider={this.layoutProvider}
        rowRenderer={this.rowRenderer}
        renderAheadOffset={250}
        scrollViewProps={{
          showsVerticalScrollIndicator: false
        }}
      />
    );
  }
}

export default SearchListRenderer;
