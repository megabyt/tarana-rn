import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  listRendererContainer: { marginHorizontal: 4, paddingVertical: 4, display: 'flex', flex: 1 }
});
