export interface TagDataEntity {
  popularity: number;
  stations: number;
  tag_id: string;
  tag_name: string;
}

export interface TagTypes {
  data?: TagDataEntity[];
  length: number;
  message: string;
  status: boolean;
}
