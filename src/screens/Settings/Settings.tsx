import {
  Add,
  DocumentText1,
  Facebook,
  Location,
  MessageText1,
  Moon,
  Send2,
  Star1,
  Sun1
} from 'iconsax-react-native';
import React from 'react';
import { ScrollView, View } from 'react-native';
import SettingsOption from '~components/SettingsOption/SettingsOption';
import MbText from '~components/shared/MbText/MbText';
import { COLORS } from '~config/config';
import Main from '~screens/Main';
import { globalStyles } from '~styles/global';
import { styles } from './settings.style';

const Settings = () => (
  <ScrollView
    contentContainerStyle={styles.settingsScrollViewContainerStyle}
    showsVerticalScrollIndicator={false}
  >
    <Main index={0}>
      <View style={styles.settingsOptionContainer}>
        <MbText style={[globalStyles.titleStyle, styles.settingsOptionHeading]}>Theme</MbText>
        <SettingsOption
          type="switch"
          icon1={<Sun1 color={COLORS.dark[500]} size={30} />}
          icon2={<Moon color={COLORS.dark[500]} size={30} />}
        />
      </View>
      <View style={styles.settingsOptionContainer}>
        <MbText style={[globalStyles.titleStyle, styles.settingsOptionHeading]}>Location</MbText>
        <SettingsOption
          type="text"
          icon1={<Location color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          content="INDIA"
          menuTitle="Country"
        />
      </View>
      <View style={styles.settingsOptionContainer}>
        <MbText style={[globalStyles.titleStyle, styles.settingsOptionHeading]}>
          Terms & Conditions
        </MbText>
        <SettingsOption
          type="rightIcon"
          icon1={<DocumentText1 color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          menuTitle="Privacy Policy"
        />
        <SettingsOption
          type="rightIcon"
          icon1={<DocumentText1 color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          menuTitle="Terms of Services"
        />
      </View>

      <View style={styles.settingsOptionContainer}>
        <MbText style={[globalStyles.titleStyle, styles.settingsOptionHeading]}>
          Feedback & Socials
        </MbText>
        <SettingsOption
          type="rightIcon"
          icon1={<MessageText1 color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          menuTitle="Contact Us"
        />
        <SettingsOption
          type="rightIcon"
          icon1={<Send2 color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          menuTitle="Join us on Telegram"
        />
        <SettingsOption
          type="rightIcon"
          icon1={<Facebook color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          menuTitle="Join us on Facebook"
        />

        <SettingsOption
          type="rightIcon"
          icon1={<Star1 color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          menuTitle="Rate Tarana"
        />
        <SettingsOption
          type="rightIcon"
          icon1={<Add color={COLORS.dark[500]} size={30} />}
          icon2={<></>}
          menuTitle="Request a Station"
        />
      </View>
      <View style={[styles.settingsOptionContainer, styles.appVersionContainer]}>
        <MbText style={[globalStyles.subTitleStyle]}>v1.0.21</MbText>
      </View>
    </Main>
  </ScrollView>
);

export default Settings;
