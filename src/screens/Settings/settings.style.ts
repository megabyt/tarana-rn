import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  settingsOptionContainer: {
    marginVertical: 8
  },
  settingsOptionHeading: {
    marginLeft: 30
  },
  settingsScrollViewContainerStyle: {
    paddingVertical: 20
  },
  appVersionContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
