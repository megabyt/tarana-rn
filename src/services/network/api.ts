import { create } from 'apisauce';

export const api = create({
  baseURL: 'http://152.67.2.105:3000/v1',
  headers: {
    'Content-Type': 'application/json; charset=utf-8'
  }
});
