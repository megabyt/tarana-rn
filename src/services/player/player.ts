import TrackPlayer, { Capability, State, TrackType } from 'react-native-track-player';
import { CurrentTrackType } from '~components/Player/types';
import { DataEntity } from '~screens/Home/cleanedHomeData.types';
import useStore from '~states/useStore';
import { getTrackDetailsFromItemData } from '~utils/playerUtils';

const playerService: any = {};
const { hasPlayerSetup, setHasPlayerSetup, setTrackType, trackType, setCurrentTrack } =
  useStore.getState();
playerService.setup = async () => {
  try {
    await TrackPlayer.setupPlayer();
    await TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        Capability.Play,
        Capability.Pause,
        Capability.SkipToNext,
        Capability.SkipToPrevious,
        Capability.Stop
      ],
      compactCapabilities: [
        Capability.Play,
        Capability.Pause,
        Capability.SkipToNext,
        Capability.Stop
      ]
    });
    setHasPlayerSetup(true);
    return true;
  } catch (error) {
    setHasPlayerSetup(false);
    return false;
  }
};

playerService.addTrack = async (track: CurrentTrackType) => {
  let type = TrackType.Default;
  try {
    if (track.url.includes('.m3u8')) {
      type = TrackType.HLS;
    }

    if (track.radio_uid) {
      setTrackType('radio');
    }
    await TrackPlayer.reset();
    await TrackPlayer.add({
      id: 'tarana-audio',
      url: track.url,
      title: track.name,
      artist: 'Tarana Radio',
      artwork: track.posterUrl,
      type,
      icon: track.iconUrl,
      radio_uid: track.radio_uid
    });
    setCurrentTrack(track);
    return true;
  } catch (error) {
    setCurrentTrack({});
    return true;
  }
};

playerService.play = async (track: CurrentTrackType) => {
  await playerService.addTrack(track);
  await TrackPlayer.play();
};

playerService.playItem = async (item: DataEntity) => {
  if (!hasPlayerSetup) {
    await playerService.setup();
  }
  const track = await getTrackDetailsFromItemData(item);
  if (!track) {
    return;
  }
  await playerService.play(track);
  // await playerService.togglePlay();
};

playerService.togglePlay = async () => {
  const state = await TrackPlayer.getState();

  if (state === State.Playing) {
    TrackPlayer.pause();
  } else {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack.toString()) {
      if (trackType === 'radio') {
        TrackPlayer.seekTo(0);
      }
      TrackPlayer.play();
    } else {
      // player.playLastPlayed('player_buttons');
    }
    // events.logEvent('station_played');
  }
};

playerService.setVolume = async (volume: number) => {
  TrackPlayer.setVolume(Number(volume));
};

export default playerService;
