export interface ItemType {
  station_uid: string;
  name: string;
  radio_uid: string;
  likes: number;
  popularity: number;
  frequency: string;
  categories: string;
  website: string;
  city: string;
  state: string;
  country: string;
  streams: string;
  icon_image: string;
  player_image: string;
}
