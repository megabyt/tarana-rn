/* eslint-disable @typescript-eslint/no-unused-vars */
import { StoreSlice } from '../useStore';
import { BaseSliceType } from './types';

const createBaseSlice: StoreSlice<BaseSliceType> = (set, get) => ({
  keyboardState: false,
  isSearchBarFocussed: false,
  searchTerm: '',

  setIsKeyboardOpen: (state: boolean) => {
    set((prev: BaseSliceType) => ({ ...prev, keyboardState: state }));
  },
  setSearchBarFocusStatus: (state: boolean) => {
    set((prev: BaseSliceType) => ({ ...prev, isSearchBarFocussed: state }));
  },
  setSearchTerm: (term: string) => {
    set((prev: BaseSliceType) => ({ ...prev, searchTerm: term }));
  }
});
export default createBaseSlice;
