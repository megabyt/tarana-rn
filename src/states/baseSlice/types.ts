export interface BaseSliceType {
  keyboardState: boolean;
  isSearchBarFocussed: boolean;
  searchTerm: string;
  setIsKeyboardOpen: (state: boolean) => void;
  setSearchBarFocusStatus: (state: boolean) => void;
  setSearchTerm: (term: string) => void;
}
