/* eslint-disable @typescript-eslint/no-unused-vars */
import { CurrentTrackType } from '~components/Player/types';
import { StoreSlice } from '../useStore';
import { PlayerSliceTypes } from './types';

const createPlayerSlice: StoreSlice<PlayerSliceTypes> = (set, get) => ({
  isPlayerModalVisible: false,
  needMiniPlayer: true,
  hasPlayerSetup: false,
  trackType: 'default',
  playerVolume: 0.5,
  playerProgress: 0.0,
  currentTrack: {} as CurrentTrackType,

  setIsPlayerModalVisible: (state: boolean) => {
    set((prev: PlayerSliceTypes) => ({ ...prev, isPlayerModalVisible: state }));
  },
  setNeedMiniPlayer: (state: boolean) => {
    set((prev: PlayerSliceTypes) => ({ ...prev, needMiniPlayer: state }));
  },
  setHasPlayerSetup: (state: boolean) => {
    set((prev: PlayerSliceTypes) => ({ ...prev, hasPlayerSetup: state }));
  },
  setTrackType: (state: string) => {
    set((prev: PlayerSliceTypes) => ({ ...prev, trackType: state }));
  },
  setCurrentTrack: (track: CurrentTrackType) => {
    set((prev: PlayerSliceTypes) => ({ ...prev, currentTrack: track }));
  },
  setPlayerVolume: (volume: number) => {
    set((prev: PlayerSliceTypes) => ({ ...prev, playerVolume: volume }));
  },
  setPlayerProgress: (progress: number) => {
    set((prev: PlayerSliceTypes) => ({ ...prev, playerProgress: progress }));
  }
});
export default createPlayerSlice;
