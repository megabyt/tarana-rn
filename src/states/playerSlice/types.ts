import { CurrentTrackType } from '~components/Player/types';

export interface PlayerSliceTypes {
  isPlayerModalVisible: boolean;
  needMiniPlayer: boolean;
  hasPlayerSetup: boolean;
  trackType: string;
  currentTrack: CurrentTrackType;
  playerVolume: number;
  playerProgress: number;
  setIsPlayerModalVisible: (state: boolean) => void;
  setNeedMiniPlayer: (state: boolean) => void;
  setHasPlayerSetup: (state: boolean) => void;
  setTrackType: (type: string) => void;
  setCurrentTrack: (track: CurrentTrackType) => void;
  setPlayerVolume: (volume: number) => void;
  setPlayerProgress: (progress: number) => void;
}
