import { StyleSheet } from 'react-native';
import { FONTS } from '~config/config';

export const globalStyles = StyleSheet.create({
  textStyle: {
    fontSize: FONTS.sizeSR,
    fontFamily: FONTS.medium
  },
  subTitleStyle: {
    fontSize: FONTS.sizeSR,
    fontFamily: FONTS.medium
  },
  tabBarLabelStyles: {
    fontFamily: FONTS.medium,
    fontSize: FONTS.sizeS,
    paddingBottom: 2
  },
  titleStyle: {
    fontFamily: FONTS.semiB,
    fontSize: FONTS.sizeXR,
    paddingVertical: 10
  },
  cricketHeadingStyle: {
    fontFamily: FONTS.semiB,
    fontSize: FONTS.sizeR
  },
  textSmallBold: {
    fontSize: FONTS.sizeSR,
    fontFamily: FONTS.bold
  }
});
