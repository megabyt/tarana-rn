/* eslint-disable no-bitwise */
import ImageColors from 'react-native-image-colors';

const sortBy = require('lodash.sortby');
export const isObject = require('lodash.isobject');
const orderBy = require('lodash.orderby');
const escapeRegExp = require('lodash.escaperegexp');

export const objToSortedArr = (obj: any) => {
  const sortedArrData: any = [];
  sortBy(obj, ['order']).map((el: any) => isObject(el) && sortedArrData.push(el));
  return sortedArrData;
};

export const orderArrBy = (arr: any, predicate: string, mode: string) => {
  return orderBy(arr, predicate, mode);
};

const checkMatching = (s1: string, s2: string) =>
  s1.match(escapeRegExp(s2)) || s2.match(escapeRegExp(s1));

export const arrFilter = (arr: any, term: string) => {
  if (term.length > 1) {
    let matchingTags = arr.filter((tag: any) => {
      const tagName = tag.tag_name;
      let matched = null,
        matchLength = 0;

      tag.match_length = matchLength;

      if (typeof tagName === 'string') {
        matched = checkMatching(tagName, term);

        if (!matched) {
          const splitTerm = term.split(' ');

          if (splitTerm.length) {
            splitTerm.find((word) => {
              if (!word) {
                return false;
              }
              matched = checkMatching(word, tagName);
              return matched;
            });
          }
        }

        if (matched != null) {
          matchLength = matched[0].length;
          tag.match_length = matched[0].length;
          return true;
        }
      }
      return false;
    });

    if (matchingTags.length) {
      matchingTags = orderBy(
        matchingTags,
        ['match_length', 'popularity', 'tag_name'],
        ['desc', 'desc', 'desc']
      );
    }

    return matchingTags;
  }
};

export const getUniqueKey = () =>
  new Date().getTime().toString() +
  Math.floor(Math.random() * Math.floor(new Date().getTime())).toString();

export const ViewTypes = {
  FULL: 0,
  HALF_LEFT: 1,
  HALF_RIGHT: 2
};

export const getColors = async (poster: string) => {
  const results = await ImageColors.getColors(poster, {
    fallback: '#20005e',
    cache: true,
    key: getUniqueKey()
  });
  return results;
};

export function getComplementaryColor(color: string) {
  return '#' + ('000000' + (0xffffff ^ parseInt(color.substring(1), 16)).toString(16)).slice(-6);
}

export const getTimeDuration = (duration: any, position: any) => {
  if (duration) {
    return new Date((duration - position) * 1000).toISOString().slice(14, 19);
  }
  return new Date(position * 1000).toISOString().slice(14, 19);
};
