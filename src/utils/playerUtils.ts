/* eslint-disable eqeqeq */
import { isObject } from './helpers';

export const getTrackDetailsFromItemData = async (item: any) => {
  if (!isObject(item) || Object.keys(item).length === 0 || item.streams == undefined) {
    return null;
  }

  const track: any = {};

  try {
    const streams = JSON.parse(item.streams);

    if (Array.isArray(streams) && streams.length > 0) {
      track.url = streams[0];
    } else if (typeof streams === 'string' && streams !== '') {
      track.url = streams;
    }
  } catch {
    track.url = null;
  }

  track.name = item.name || 'You are listening Tarana Radio';
  track.posterUrl = item.player_image;
  track.iconUrl = item.icon_image;
  track.radio_uid = item.radio_uid;
  track.icon_image = item.icon_image;

  return track.url ? track : null;
};
